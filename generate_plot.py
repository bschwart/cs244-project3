import dpkt
import socket
from termcolor import cprint
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import struct
from argparse import ArgumentParser

parser = ArgumentParser(description="TCP Recovery")
parser.add_argument('--receiver-ip', '-r',
                    help="Receiver's IP",
                    default='10.0.0.1')

parser.add_argument('--sender-ip', '-s',
                    help="Sender's IP",
                    default='10.0.0.2')

parser.add_argument('--file-size',
                    help="File size used for this iteration of the simulation",
                    required=True)

parser.add_argument('--tcp-mode', '-m',
                    help="TCP mode running on this kernel",
                    required=True)

parser.add_argument('--dir', '-d',
                    help="Directory to store output",
                    required=True)

args = parser.parse_args()

# The plot uses time as the x-axis and byte sequence numbers for the y-axis.
# sender.una is continuous because it represents the last unacknowledged byte.
# Data sent, retransmitted, and SACK blocks are each represented by vertical
# lines at the time value and span the range of bytes included in the event. 

# Time values for the x-axis (tcpdump -tttt option)
time = [] 
# Data sent for the first time. In the format (first byte, last byte)
sent = []
# The last byte acknowledged by the receiver
acknowledged = []
# Data retransmitted. In the format (first byte, last byte)
retransmitted = []
# SACK block sent by the receiver to the sender with acks
sacked = []

ipsrc = socket.inet_aton(args.sender_ip)
ipdst = socket.inet_aton(args.receiver_ip)

def process_pcap(controller_mode):
    with open('%s/%sKB-%s-%s.pcap' % (args.dir, args.file_size, args.tcp_mode, controller_mode)) as f:
        pcap = dpkt.pcap.Reader(f)
        # Initialize values used in multiple iterations of the loop 
        tcp_start_seq = None 
        start_time = None 
        last_sent = None 
        last_ack = 0 
        done = False 
        first_send = None
        last_send = None      
 
        for ts, buf in pcap: # Loop through all packets
            ip, tcp, start_time = parse_pkt(ts, buf, start_time)
            if not ip or not tcp:
                continue
            ack_flag = ( tcp.flags & dpkt.tcp.TH_ACK ) != 0
            fin_flag = ( tcp.flags & dpkt.tcp.TH_FIN ) != 0
            sending = None
            retransmitting = None
            sack = None
            if ip.src == ipsrc and ip.dst == ipdst:
                if not tcp_start_seq:
                    tcp_start_seq = tcp.seq
                if len(tcp.data) == 0:
                    continue
                if not first_send:
                    first_send = ts 
                bytes_sent = (tcp.seq - tcp_start_seq, tcp.seq - tcp_start_seq + len(tcp.data)-1)
                if fin_flag:
                    cprint(str(bytes_sent[1] + 1) + ' bytes sent','green')
                if  last_sent == None or bytes_sent[0] > last_sent[1]: # New data
                    last_sent = bytes_sent
                    sending = bytes_sent
                elif bytes_sent[1] < last_sent[0]: # Retransmission only
                    retransmitting = bytes_sent
                else: # Retransmission and new data
                    sending = (last_sent[1] + 1, bytes_sent[1])
                    retransmitting = (bytes_sent[0], last_sent[1])
                    last_sent = sending
            elif ip.src == ipdst and ip.dst == ipsrc and ack_flag:
                if not tcp_start_seq:
                    continue 
                if fin_flag:
                    cprint(str(tcp.ack - tcp_start_seq - 1) + ' bytes received', 'green')
                    cprint("Total transfer time: %ss" % str(ts - first_send), 'green')
                    last_send = ts
                    done = True
                last_ack = tcp.ack - tcp_start_seq - 1
                for tcp_opt in dpkt.tcp.parse_opts ( tcp.opts ):
                    if(tcp_opt[0] == 5):
                        sack_block = tcp_opt[1]
                        sack_start = struct.unpack(">L",sack_block[0:4])[0]
                        sack_stop = struct.unpack(">L",sack_block[4:8])[0]
                        sack = (sack_start - tcp_start_seq, sack_stop - tcp_start_seq)
            if not done:
                time.append(ts - start_time)
                sent.append(sending)
                retransmitted.append(retransmitting)
                acknowledged.append(last_ack)
                sacked.append(sack)
        plot_result(last_send - first_send, controller_mode)

def parse_pkt(ts, buf, start_time):
    if not start_time:
        start_time = ts
    ip = None
    tcp = None
    eth = dpkt.ethernet.Ethernet(buf)
    if eth.type == dpkt.ethernet.ETH_TYPE_IP:
        ip = eth.data
        if ip.p == dpkt.ip.IP_PROTO_TCP:
            tcp = ip.data
    return (ip, tcp, start_time) 

def plot_result(total_time, controller_mode):
    fig = plt.figure()
    for t in range(0, len(time)):
        plot_range(time[t], sent[t], u'k')
        plot_range(time[t], retransmitted[t], u'r')
        plot_range(time[t], sacked[t], colors.hex2color('#8A2BE2'))
        plt.plot(time, acknowledged, 'g')
    plt.ylabel('Bytes')
    plt.xlabel('Time (s)')
    plt.title('%sKB file transfer using %s (%0.2fs total time)' % (args.file_size, args.tcp_mode, total_time))
    fig.savefig('%s/%sKB-%s-%s.png' % (args.dir, args.file_size, args.tcp_mode, controller_mode))
    fig.clear()

def plot_range(t, r, c):
    if r:
        plt.vlines(t, r[0], r[1], c)

if __name__ == "__main__":
    for controller_mode in ['Basic-Loss','Light-Loss','Heavy-Loss']:
        print "Plotting %s" % controller_mode
        time[:] = []
        sent[:] = []
        retransmitted[:] = []
        sacked[:] = []
        acknowledged[:] = []
        process_pcap(controller_mode)
