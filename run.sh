#!/bin/bash
if [ $EUID -ne 0 ]; then
   echo "Mininet must be run as root." 1>&2
   exit 1
fi

# Use custom controller
cp tcpctl_light.py ~/pox/pox/forwarding/tcpctl_light.py
cp tcpctl_heavy.py ~/pox/pox/forwarding/tcpctl_heavy.py
cp tcpctl.py ~/pox/pox/forwarding/tcpctl.py

# Use kernel to determine tcp mode
if [ "$(uname -r)" == "3.0.0-30-virtual" ]; then
    tcp_mode="NoPRR"
elif [ "$(uname -r)" == "3.2.0-79-virtual" ]; then
    tcp_mode="PRR"
else
    tcp_mode="UNKNOWN_TCP"
fi

test_date=$(date +%Y-%m-%d-%H\:%M\:%S)
dir=tcp-recovery-"$test_date"
for file_size in 32 64; do
    dd if=/dev/zero of=file.txt count=$file_size bs=1024 &> /dev/null
    echo Running $tcp_mode for a "$file_size"KB file transfer
    python tcp_recovery.py --dir $dir --tcp-mode $tcp_mode --bw-net 100 --delay 10 --file-size $file_size
    python generate_plot.py --dir $dir --file-size $file_size --tcp-mode $tcp_mode 
    rm file.txt
done
