#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.node import * #added all nodes - for ovs & controller
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import custom, dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

from termcolor import cprint

import sys
import os
import math

class POXBridge_base ( Controller ):
    "customized controller class to invoke POX forwarding.tcpctl"
    def start( self ):
        "start customized POX switch"
        self.pox = '%s/pox/pox.py' % os.environ[ 'HOME']
        self.cmd (self.pox, 'forwarding.tcpctl &')

    def stop( self ):
        "stop controller"
        self.cmd( 'kill %' + self.pox)

class POXBridge_light ( Controller ):
    "customized controller class to invoke POX forwarding.tcpctl"
    def start( self ):
        "start customized POX switch"
        self.pox = '%s/pox/pox.py' % os.environ[ 'HOME']
        self.cmd (self.pox, 'forwarding.tcpctl_light &')

    def stop( self ):
        "stop controller"
        self.cmd( 'kill %' + self.pox)

class POXBridge_heavy ( Controller ):
    "customized controller class to invoke POX forwarding.tcpctl"
    def start( self ):
        "start customized POX switch"
        self.pox = '%s/pox/pox.py' % os.environ[ 'HOME']
        self.cmd (self.pox, 'forwarding.tcpctl_heavy &')

    def stop( self ):
        "stop controller"
        self.cmd( 'kill %' + self.pox)

#controllers = {'poxbridge': POXBridge}

parser = ArgumentParser(description="TCP Recovery")
parser.add_argument('--bw-host', '-B',
                    type=float,
                    help="Bandwidth of host links (Mb/s)",
                    default=1000)

parser.add_argument('--bw-net', '-b',
                    type=float,
                    help="Bandwidth of bottleneck (network) link (Mb/s)",
                    required=True)

parser.add_argument('--delay',
                    type=float,
                    help="Link propagation delay (ms)",
                    required=True)

parser.add_argument('--dir', '-d',
                    help="Directory to store output",
                    required=True)

parser.add_argument('--file-size', '-f',
                    help="File size used for the test",
                    required=True)

parser.add_argument('--tcp-mode', '-m',
                    help="TCP mode running on this kernel",
                    required=True)

parser.add_argument('--cong',
                    help="Congestion control algorithm to use",
                    default="reno")

parser.add_argument('--time', '-t',
                    help="Duration (sec) to run the experiment",
                    type=int,
                    default=10)

parser.add_argument('--maxq',
                    type=int,
                    help="Max buffer size of network interface in packets",
                    default=100)

args = parser.parse_args()


class TCPRecoveryTopo(Topo):
    "Simple topology for TCP recovery experiment."

    def build(self, n=2):
        # TODO: create two hosts
        host1 = self.addHost('h1')
        host2 = self.addHost('h2')

        # Here I have created a switch.  If you change its name, its
        # interface names will change from s0-eth1 to newname-eth1.
        switch = self.addSwitch('s0')

        # TODO: Add links with appropriate characteristics
        my_delay = str(args.delay) + "ms"
        self.addLink(host1, switch, bw=args.bw_host, delay=my_delay)
        self.addLink(switch, host2, bw=args.bw_net, delay=my_delay, max_queue_size=args.maxq)

        return

def start_receiver(net):
    h = net.get('h1')
    print "Starting receiver..."
    h.popen("nc -l %d > %s/received" % (23000, args.dir), shell=True)

def stop_receiver(net):
    h = net.get('h1')
    print "Stopping receiver..."
    h.cmd("killall -9 nc")
    h.cmd("rm -f %s/received" % args.dir)
    h.cmd("killall -s SIGINT tcpdump")

def run_sender(net, controller_mode):
    h = net.get('h2')
    receiver = net.get('h1')
    print "Transferring file..."
    h.popen("tcpdump -i h2-eth0 -ttttn -l -w %s/%sKB-%s-%s.pcap tcp" % (args.dir, args.file_size, args.tcp_mode, controller_mode), shell=True)
    h.cmd("nc %s %d < file.txt" % (receiver.IP(), 23000))

def tcp_recovery():
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    os.system("chown ubuntu %s" % args.dir)
    os.system("sysctl -w net.ipv4.tcp_congestion_control=%s > /dev/null 2>&1" % args.cong)
    topo = TCPRecoveryTopo()
    for controller_mode in [(POXBridge_base, 'Basic-Loss'), (POXBridge_light, 'Light-Loss'), (POXBridge_heavy, 'Heavy-Loss')]:
        cprint("Running experiment with %s controller" % controller_mode[1], 'red')
        net = Mininet(topo=topo, host=custom(CPULimitedHost, cpu=.1, sched='rt'), link=TCLink, switch=OVSSwitch, controller=controller_mode[0])
        net.start()
        start_receiver(net)
        run_sender(net, controller_mode[1])
        sleep(3)
        stop_receiver(net)
        net.stop()

if __name__ == "__main__":
    tcp_recovery()
