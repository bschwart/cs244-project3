Step 1:

Launch 2 EC2 instances in us-east-1 (N. Virginia) region.
You can use any instance size larger than the t1/t2 family. (We used m3.medium)
Please make sure your security group has port 22 open to your IP address.
Additionally, it is easy to view the graphs if you open port 8000 to TCP
traffic for the python simple HTTP server.

The Amazon Machine Images listed below can be found under community AMIs:
Instance 1: ami-13ba2d7a
Instance 2: ami-00615068

Step 2:
ssh to each of the instances and copy the following text into a file using
the text editor of your choosing (e.g. nano, vim). Then run it as a script.
If you named the file setup.sh then run the command: bash setup.sh
Please note that this script takes 10-15 minutes to run.

--------------- Start Script - Start Copying After This Line ------------------
#!/bin/bash
no_prr=ami-13ba2d7a
prr=ami-00615068

echo "Detected Ubuntu $(lsb_release -r) with kernel $(uname -r)"
if [ "$no_prr" == "$(ec2metadata --ami-id)" ]; then
    echo "This is the AMI for No PRR"
    # This release is no longer supported
    sudo sed -i -e 's/us-east-1.ec2.archive.ubuntu.com\|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list
    sudo apt-get update
    sudo apt-get -y install linux-headers-3.0.0-30-virtual
elif [ "$prr" == "$(ec2metadata --ami-id)" ]; then
    echo "This is the AMI for PRR"
    sudo apt-get update
else
    echo "This is an unrecognized AMI - this script may or may not work"
    sudo apt-get update
fi

# Install dependencies
sudo apt-get -y install git
sudo apt-get -y build-dep python-matplotlib
sudo apt-get -y install python-pip
sudo pip install dpkt
sudo pip install --upgrade numpy
sudo easy_install -U distribute
sudo pip install matplotlib
sudo pip install termcolor


# Install Mininet
cd /home/ubuntu
git clone git://github.com/mininet/mininet
cd mininet
git checkout -b 2.2.1 2.2.1
cd ..
mininet/util/install.sh -a
sudo mn --test pingall

# Setup experiment directory
git clone git@bitbucket.org:bschwart/cs244-project3.git
cd cs244-project3
mod +x run.sh

--------------- Stop Script - Stop Copying After This Line ------------------

Step 3:
Run the command:
sudo ./run.sh

Step 4:
Start the python HTTP server:
python -m SimpleHTTPServer

Step 5:
View the plots by navigating to:
http://<EC2 Instance IP Address>:8000
and navigating to the tcp-recovery-<date>-<time> directory

You can click on any of the .png files to view a graph

Other notes:
The cleanup.sh script is an easy way to delete all results in the directory.
It also runs Mininet cleanup in case the run.sh script was interrupted.
